#!/bin/sh
#
# mask.sh is a shell program to easily mask value of environment
# variables in log file.
#
# Copyright (c) 2019 FX Soubirou
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

VERSION=__VERSION__

VARIABLES_ARG=""
LOG_FILE=""
MASK="**MASKED**"
MASKED_COUNT=0
VARIABLE_INDEX=0
MANDATORY_ARGUMENT=1

usage() {
    echo "This script masks value of confidential variables in log file."
    echo ""
    echo "Usage: mask.sh -f <file_path> -m <variable1>,<variable2>"
    echo "Options:"
    echo "  -f file path of the log file"
    echo "  -m list of variables to mask separated by comma"
    echo "  -v | --version print version"
    echo "  -h print usage"
}

version() {
    year=$(date +'%Y')
    echo "$0, version $VERSION"
    echo "Copyright (C) $year FX Soubirou"
    echo "License GPLv3+ : GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>"
}

test_argument() {
    if [ -z "$2" ]; then
        echo "Error: -$1 argument is mandatory !" >&2
        MANDATORY_ARGUMENT=0
    fi
}

init_count() {
  count=$(grep -o "$MASK" "$LOG_FILE" | wc -l)
  MASKED_COUNT=$count
}

mask_value() {
  escaped=$(echo "$1" | sed -e 's/[]\/$*.^[]/\\&/g')
  sed -i -e s/"$escaped"/"$MASK"/g "$LOG_FILE"
}

display_count() {
  count=$(grep -o "$MASK" "$LOG_FILE" | wc -l)
  delta=$((count - MASKED_COUNT))
  echo "Masked variable [$VARIABLE_INDEX]: $delta matches"
  VARIABLE_INDEX=$((VARIABLE_INDEX + 1))
}

if [ "$1" = "--version" ]; then
    version
    exit 0
fi

while getopts f:hm:v option; do
    case "${option}" in
    h)
        usage
        exit 0
        ;;
    v)
        version
        exit 0
        ;;
    f)
        LOG_FILE="${OPTARG}"
        ;;
    m)
        VARIABLES_ARG="${OPTARG}"
        ;;
    *)
        usage
        exit 1
        ;;
    esac
done

test_argument "f" "$LOG_FILE"
test_argument "m" "$VARIABLES_ARG"

if [ "$MANDATORY_ARGUMENT" = "0" ]; then
    usage
    exit 1
fi

if ! [ -e "$LOG_FILE" ]; then
    echo "Error: file $LOG_FILE does not exist!" >&2
    exit 1
fi

IFS=","
for variable in $VARIABLES_ARG
do
     init_count "$variable"
     mask_value "$variable"
     display_count
done
