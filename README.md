# jfxs / robot-framework

[![Software License](https://img.shields.io/badge/license-GPL%20v3-informational.svg?style=flat&logo=gnu)](LICENSE)
[![Pipeline Status](https://gitlab.com/fxs/docker-robot-framework/badges/master/pipeline.svg)](https://gitlab.com/fxs/docker-robot-framework/pipelines)
[![Robot Framework Tests](https://fxs.gitlab.io/docker-robot-framework/all_tests.svg)](https://fxs.gitlab.io/docker-robot-framework/report.html)
[![Image size](https://fxs.gitlab.io/docker-robot-framework/docker.svg)](https://hub.docker.com/r/jfxs/robot-framework)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

A lightweight automatically updated and tested multiarch amd64 and arm64 Docker image to run [Robot Framework](https://robotframework.org/) tests and containing:

* [Robot Framework](https://github.com/robotframework/robotframework)
* [Robot Framework SeleniumLibrary](https://github.com/robotframework/SeleniumLibrary)
* [Robot Framework Requests](https://github.com/bulkan/robotframework-requests)
* mask.sh: a shell script to mask confidential variables in files.
* output2badge.sh: a shell script to generate badges from output.xml Robot Framework file.

This image is automatically updated with the [Automated Continuous Delivery of Docker images](https://medium.com/@fx_s/automated-continuous-delivery-of-docker-images-b4bfa0d09f95) pattern.

## Getting Started

### Prerequisities

In order to run this container you'll need Docker installed.

* [Windows](https://docs.docker.com/windows/started)
* [OS X](https://docs.docker.com/mac/started/)
* [Linux](https://docs.docker.com/linux/started/)

### Usage

Example to run Robot Framework tests in your current directory:

```shell
docker run -t --rm -v ${PWD}/tests:/tests:ro -v ${PWD}/reports:/reports jfxs/robot-framework robot --outputdir /reports /tests
```

Example to add a Robot Framework library and run Robot Framework tests in your current directory:

```shell
docker run -t --rm -v ${PWD}/tests:/tests:ro -v ${PWD}/reports:/reports jfxs/robot-framework /bin/sh -c "pip install --user robotframework-faker && robot --outputdir /reports /tests"
```

mask.sh is a shell program to mask confidential variables in files, especially in output.xml Robot Framework file before publishing.

```shell
Usage: mask.sh -f <file_path> -m <variable1>,<variable2>
Options:
  -f file path of the log file
  -m list of variables to mask separated by comma
  -v | --version print version
  -h print usage

Example: mask.sh -f output.xml -m myPassword
```

output2badge.sh is a shell program to generate 2 badges from output.xml Robot Framework file:

* all_tests.svg [![Robot Framework Tests](https://fxs.gitlab.io/docker-robot-framework/all_tests.svg)](https://fxs.gitlab.io/docker-robot-framework/report.html)
* critical_tests.svg [![Robot Framework Critical Tests](https://fxs.gitlab.io/docker-robot-framework/critical_tests.svg)](https://fxs.gitlab.io/docker-robot-framework/report.html)

If all tests are passed, the background will be green. If at least one none critical test is failed, the background will be orange. If at least one critical test is failed, the background will be red.

```shell
Usage: output2badge.sh -f <file_path> [-d <badges_directory>]
Options:
  -f file path of the log file
  -d badges directory, default current directory
  -v | --version print version
  -h print usage

Example: output2badge.sh -f output.xml
will generate all_tests.svg and critical_tests.svg in current directory.
```

## Built with

Docker latest tag contains:

See versions on [Dockerhub](https://hub.docker.com/r/jfxs/robot-framework)

Versions of installed software are listed in /etc/VERSIONS file of the Docker image. Example to see them for a specific tag:

```shell
docker run -t jfxs/robot-framework:3.1.2-1 cat /etc/VERSIONS
```

## Versioning

The Docker tag is defined by the Robot Framework used version and an increment to differentiate build with the same Robot Framework version:

```text
<robotframework_version>-<increment>
```

Example: 3.1.2-1

## Tests

Tests are runned with [Robot Framework](http://robotframework.org/).
Last test report is available [here](https://fxs.gitlab.io/docker-robot-framework/report.html).

## Vulnerability Scan

The Docker image is scanned every day with the open source vulnerability scanner [Trivy](https://github.com/aquasecurity/trivy).

The latest vulnerability scan report is available on [Gitlab Security Dashboard](https://gitlab.com/fxs/docker-robot-framework/-/security/dashboard/?state=DETECTED&state=CONFIRMED&reportType=CONTAINER_SCANNING).

## Find Us

* [Dockerhub](https://hub.docker.com/r/jfxs/robot-framework)
* [Gitlab](https://gitlab.com/fxs/docker-robot-framework)

## Authors

* **FX Soubirou** - *Initial work* - [Gitlab repositories](https://gitlab.com/users/fxs/projects)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. See the [LICENSE](https://gitlab.com/fxs/docker-robot-framework/blob/master/LICENSE) file for details.
