** Settings ***
Library           Collections
Library           Process
Library           RequestsLibrary
Library           SeleniumLibrary

*** Variables ***
${TESTS_DIR}   tests
${GITLAB_API_VERSION}   v4
${GITLAB_USERNAME}   fxs
${GITLAB_NAME}   jfxs
${GRID_URL}   http://selenium:4444/wd/hub

*** Test Cases ***
Test Robot Framework: [--version] option
    [Tags]    core    image
    ${result} =    When Run Process    robot   --version
    Then Should Be Equal As Integers    ${result.rc}    251
    And Should Contain    ${result.stdout}    Robot Framework

Test Requests library
    [Tags]    request    image
    ${resp}=   When GET   https://gitlab.com/api/${GITLAB_API_VERSION}/users   params=username=${GITLAB_USERNAME}   expected_status=200
    ${user}=   Then Get From List   ${resp.json()}   0
    And Dictionary Should Contain Item   ${user}   name   ${GITLAB_NAME}

Test Selenium library
    [Tags]    selenium    image
    Given Open Browser	 https://duckduckgo.com/   Chrome 	remote_url=${GRID_URL}
    And Set Screenshot Directory	 EMBED
    And Wait Until Page Contains Element  id=search_form_input_homepage
    When Input text   id=search_form_input_homepage   robot framework
    And Wait Until Element Is Enabled  id=search_button_homepage
    And Scroll Element Into View   id=search_button_homepage
    And Click Element   id=search_button_homepage
    Then Wait Until Page Contains   Robot Framework
    And Capture Page Screenshot
    [Teardown]  Close All Browsers

Test library import
    [Tags]    fakerlibrary    image
    When Import Library   FakerLibrary
    ${words} =    FakerLibrary.Words
    Then Log   words: ${words}

*** Keywords ***
