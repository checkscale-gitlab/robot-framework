** Settings ***
Documentation     output2badge.sh tests
...
...               A test suite to validate output2badge shell script.

Library           OperatingSystem
Library           Process
Library           String

*** Variables ***
${TESTS_DIR}   tests
${SHELL_DIR}   files
${SHELL.SH}   output2badge.sh
${USAGE_DISPLAY}   Usage: ${SHELL.SH} -f
${COLOR_GREEN}    \#4C1
${COLOR_ORANGE}   \#FE7D37
${COLOR_RED}      \#C00
${BADGE_ALL}      all_tests.svg
${BADGE_CRITICAL}   critical_tests.svg

*** Test Cases ***
with no option
    [Tags]    output2badge    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -f argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

with unknown option
    [Tags]    output2badge    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -x
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Illegal option -x
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-f] option missing
    [Tags]    output2badge    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -d   /tmp
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    -f argument is mandatory
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-f] option with missing argument
    [Tags]    output2badge   simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -d   /tmp   -f
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

[-f] option argument with wrong file path
    [Tags]    output2badge   simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   /tmp/wrongFile.xml   -d   /tmp
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    Error: file /tmp/wrongFile.xml does not exist!

[-v] option
    [Tags]    output2badge    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   -v
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[--version] option
    [Tags]    output2badge    simple
    ${result} =    When Run Process    ${SHELL_DIR}/${SHELL.SH}   --version
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${SHELL.SH}, version

[-h] option
    [Tags]    output2badge    simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -h
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    ${USAGE_DISPLAY}

All tests are passed
    [Tags]    output2badge   simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${TESTS_DIR}/files/output-ref.xml   -d   /tmp
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    Critical Tests: 5 / 5
    And Should Contain    ${result.stdout}    All Tests: 5 / 5
    And Should Contain    ${result.stdout}    ${COLOR_GREEN}
    And badge displays   /tmp/${BADGE_CRITICAL}    5 / 5 passed    ${COLOR_GREEN}
    And badge displays   /tmp/${BADGE_ALL}    5 / 5 passed    ${COLOR_GREEN}

One none critical test is failed
    [Tags]    output2badge   simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${TESTS_DIR}/files/output-1-failed.xml   -d   /tmp
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    Critical Tests: 5 / 5
    And Should Contain    ${result.stdout}    All Tests: 6 / 7
    And Should Contain    ${result.stdout}    ${COLOR_ORANGE}
    And badge displays   /tmp/${BADGE_CRITICAL}    5 / 5 passed    ${COLOR_ORANGE}
    And badge displays   /tmp/${BADGE_ALL}    6 / 7 passed    ${COLOR_ORANGE}

Two critical tests are failed
    [Tags]    output2badge   simple
    ${result} =   When Run Process    ${SHELL_DIR}/${SHELL.SH}   -f   ${TESTS_DIR}/files/output-2-critical-failed.xml   -d   /tmp
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    Critical Tests: 3 / 5
    And Should Contain    ${result.stdout}    All Tests: 6 / 8
    And Should Contain    ${result.stdout}    ${COLOR_RED}
    And badge displays   /tmp/${BADGE_CRITICAL}    3 / 5 passed    ${COLOR_RED}
    And badge displays   /tmp/${BADGE_ALL}    6 / 8 passed    ${COLOR_RED}

*** Keywords ***
badge displays
    [Arguments]    ${file}    ${text}    ${color}
    File Should Exist   ${file}
    ${content} =   Get File    ${file}
    Should Contain   ${content}    ${text}
    Should Contain   ${content}    ${color}

